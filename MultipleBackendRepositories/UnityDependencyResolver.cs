﻿using System;
using Microsoft.Practices.Unity;
using MultipleBackendRepositories.Domain;
using MultipleBackendRepositories.Domain.Implementations;
using MultipleBackendRepositories.Infrastructure.Cache;
using MultipleBackendRepositories.Infrastructure.Database;
using MultipleBackendRepositories.Infrastructure.EF;

namespace MultipleBackendRepositories
{
    public class UnityDependencyResolver
    {
        private static Lazy<IUnityContainer> _container = new Lazy<IUnityContainer>(() =>
        {
            return new UnityContainer()
                .RegisterType<IUnitOfWork, CacheUnitOfWork>(
                    new ContainerControlledLifetimeManager(), 
                    new InjectionConstructor(
                        new InjectionParameter(DateTimeOffset.MaxValue),        // Set cache to not expire. Change date to set expiration
                        new ResolvedParameter(typeof(SqlServerUnitOfWork))))    // Inject SqlServerUnitOfWork as source to cache
                //.RegisterType<IUnitOfWork, SqlServerUnitOfWork>()             // Uncomment to switch to MS SQL backend
                .RegisterType<IMenuItemRepository, MenuItemRepository>()
                .RegisterType<IPaymentRepository, PaymentRepository>()
                ;
        });

        public static IUnityContainer Container
        {
            get { return _container.Value; }
        }
    }
}

﻿using System;
using Microsoft.Practices.Unity;
using MultipleBackendRepositories.Domain;

namespace MultipleBackendRepositories
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var menuItemName = "Menu Item 1";
                var paymentId = Guid.Parse("{32B0AB88-849A-4052-BD42-DD6670CAFE77}");

                var menuItemRepository = UnityDependencyResolver.Container.Resolve<IMenuItemRepository>();
                var menuItem = menuItemRepository.GetActiveMenuItemByName(menuItemName);

                Console.WriteLine(menuItem.Name);

                var paymentRepository = UnityDependencyResolver.Container.Resolve<IPaymentRepository>();
                var payment = paymentRepository.Get(paymentId);

                Console.WriteLine(payment.Name);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.ReadKey();
        }
    }
}

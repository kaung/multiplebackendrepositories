﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using MultipleBackendRepositories.Domain;
using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.Infrastructure.Cache
{
    /// <summary>
    /// 2PC cache unit of work. Queue the actions then execute on commit.
    /// </summary>
    public sealed class CacheUnitOfWork : IUnitOfWork
    {
        // Store transaction in the queue then execute on Commit()
        private readonly Queue<Action> _queue; 
        private readonly ObjectCache _cache;
        private readonly DateTimeOffset _expireCache;
        private readonly IUnitOfWork _source;

        /// <summary>
        /// Instantiate a cache unit of work using various caching implementations with TTL for current cache
        /// </summary>
        /// <param name="expireCache">Length of time to keep items in cache</param>
        /// <param name="sourceToCache">Unit of work to get source data to cache</param>
        public CacheUnitOfWork(DateTimeOffset expireCache, IUnitOfWork sourceToCache)
        {
            if (expireCache < DateTimeOffset.Now)
                throw new ArgumentException("expireCache cannot be set to a time in the past");

            _source = sourceToCache;
            _cache = MemoryCache.Default;
            _expireCache = expireCache;
            _queue = new Queue<Action>();

            RefreshCache();
        }

        #region IUnitOfWork Implementations

        public void Add<TEntity>(TEntity obj) where TEntity : class
        {
            Console.WriteLine("Adding to cache");
            _queue.Enqueue(() => AddToCache(obj));
        }

        public void Remove<TEntity>(TEntity obj) where TEntity : class
        {
            Console.WriteLine("Removing from cache");
            _queue.Enqueue(() => RemoveFromCache(obj));
        }

        public void Update<TEntity>(TEntity obj) where TEntity : class
        {
            Console.WriteLine("Updating cache");
            _queue.Enqueue(() => UpdateCache(obj));
        }

        public IQueryable<TEntity> GetAll<TEntity>() where TEntity : class
        {
            Console.WriteLine("Get from cache");
            var key = GetKey(typeof(TEntity)); 
            var entities = (List<TEntity>)_cache[key];

            return entities.AsQueryable();
        }

        public void Commit()
        {
            while (_queue.Count > 0)
            {
                var action = _queue.Dequeue();
                action.Invoke();
            }
        }

        public void Rollback()
        {
            _queue.Clear();
        }

        #endregion

        private void AddToCache<TEntity>(TEntity obj) where TEntity : class
        {
            var key = GetKey(typeof(TEntity));
            var entities = (List<TEntity>)_cache[key];

            if (entities.Contains(obj))
                throw new InvalidOperationException("Duplicate entity found.");

            entities.Add(obj);
        }

        private void RemoveFromCache<TEntity>(TEntity obj) where TEntity : class 
        {
            var key = GetKey(typeof(TEntity));
            var entities = (List<TEntity>)_cache[key];
            entities.Remove(obj);
        }

        private void UpdateCache<TEntity>(TEntity obj) where TEntity : class 
        {
            var key = GetKey(typeof(TEntity));
            var entities = (List<TEntity>)_cache[key];

            var existingEntity = entities.Find(x => x == obj);
            if (existingEntity == null)
                throw new InvalidOperationException("Entity not found.");

            entities.Remove(existingEntity);
            entities.Add(obj);
        }

        private string GetKey(Type type)
        {
            return type.FullName;
        }

        /// <summary>
        /// Refresh cache with repositories from source
        /// </summary>
        private void RefreshCache()
        {
            _cache.Set(GetKey(typeof(MenuItem)), _source.GetAll<MenuItem>().ToList(), _expireCache);
            _cache.Set(GetKey(typeof(Payment)), _source.GetAll<Payment>().ToList(), _expireCache);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MultipleBackendRepositories.Domain;
using MultipleBackendRepositories.Domain.Implementations;
using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.UnitTests.Domain.Implementations
{
    [TestClass]
    public class PaymentRepositoryTest
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private PaymentRepository _target;

        [TestInitialize]
        public void Setup()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _target = new PaymentRepository(_unitOfWork.Object);
        }

        [TestMethod]
        public void Get_Gets_The_Correct_Payment()
        {
            var expected = new Payment { Id = Guid.NewGuid() };
            var actual = new Payment();

            _unitOfWork.Setup(u => u.GetAll<Payment>()).Returns(new List<Payment> { expected }.AsQueryable());

            actual = _target.Get(expected.Id);

            Assert.AreSame(expected, actual, "Incorrect Payment.");
        }

        [TestMethod]
        public void GetAll_Gets_All_Payments()
        {
            var expected = new List<Payment> { new Payment(), new Payment(), new Payment() };
            var actual = new List<Payment>();

            _unitOfWork.Setup(u => u.GetAll<Payment>()).Returns(expected.AsQueryable());

            actual = _target.GetAll().ToList();

            CollectionAssert.AreEqual(expected, actual, "Incorrect Payments.");
        }
    }
}

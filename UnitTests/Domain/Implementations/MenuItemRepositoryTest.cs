﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MultipleBackendRepositories.Domain;
using MultipleBackendRepositories.Domain.Implementations;
using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.UnitTests.Domain.Implementations
{
    [TestClass]
    public class MenuItemRepositoryTest
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private MenuItemRepository _target;

        [TestInitialize]
        public void Setup()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _target = new MenuItemRepository(_unitOfWork.Object);
        }

        [TestMethod]
        public void GetActiveMenuItemByName_Returns_First_Active_Item_Matched_By_Name()
        {
            var expected = new MenuItem { IsActive = true, Name = "Test Item" };
            var actual = new MenuItem();

            _unitOfWork.Setup(u => u.GetAll<MenuItem>())
                       .Returns(new List<MenuItem>
                       {
                           expected,
                           new MenuItem { IsActive = true, Name = "Test Item" },
                           new MenuItem { IsActive = false }
                       }.AsQueryable());

            actual = _target.GetActiveMenuItemByName(expected.Name);

            Assert.AreSame(expected, actual, "Incorrect MenuItem.");
        }

        [TestMethod]
        public void Get_Gets_The_Correct_Menu_Item()
        {
            var expected = new MenuItem { Id = Guid.NewGuid() };
            var actual = new MenuItem();

            _unitOfWork.Setup(u => u.GetAll<MenuItem>()).Returns(new List<MenuItem> { expected }.AsQueryable());

            actual = _target.Get(expected.Id);

            Assert.AreSame(expected, actual, "Incorrect MenuItem.");
        }

        [TestMethod]
        public void GetAll_Gets_All_Menu_Items()
        {
            var expected = new List<MenuItem> { new MenuItem(), new MenuItem(), new MenuItem() };
            var actual = new List<MenuItem>();

            _unitOfWork.Setup(u => u.GetAll<MenuItem>()).Returns(expected.AsQueryable());

            actual = _target.GetAll().ToList();

            CollectionAssert.AreEqual(expected, actual, "Incorrect MenuItems.");
        }
    }
}

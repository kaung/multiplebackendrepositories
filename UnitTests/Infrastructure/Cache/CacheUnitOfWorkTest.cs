﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ploeh.AutoFixture;
using MultipleBackendRepositories.Domain;
using MultipleBackendRepositories.Infrastructure.Cache;
using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.UnitTests.Infrastructure.Cache
{
    [TestClass]
    public class CacheUnitOfWorkTest
    {
        private static Fixture _fixture;
        private Mock<IUnitOfWork> _unitOfWork;
        private CacheUnitOfWork _target;

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            _fixture = new Fixture();
        }

        [TestInitialize]
        public void Setup()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _target = new CacheUnitOfWork(DateTimeOffset.MaxValue, _unitOfWork.Object);
        }

        [TestMethod]
        public void Constructs_With_Source_Data_Cached_In_Memory()
        {
            var expectedMenuItems = _fixture.Create<List<MenuItem>>();
            var expectedPayments = _fixture.Create<List<Payment>>();
            var actualMenuItems = new List<MenuItem>();
            var actualPayments = new List<Payment>();

            _unitOfWork.Setup(u => u.GetAll<MenuItem>()).Returns(expectedMenuItems.AsQueryable());
            _unitOfWork.Setup(u => u.GetAll<Payment>()).Returns(expectedPayments.AsQueryable());

            _target = new CacheUnitOfWork(DateTimeOffset.MaxValue, _unitOfWork.Object);

            actualMenuItems = _target.GetAll<MenuItem>().ToList();
            actualPayments = _target.GetAll<Payment>().ToList();

            Assert.AreEqual(expectedMenuItems.Count, actualMenuItems.Count, "Failed to cache all MenuItems.");
            Assert.AreEqual(expectedPayments.Count, actualPayments.Count, "Failed to cache all Payments.");
        }
    }
}

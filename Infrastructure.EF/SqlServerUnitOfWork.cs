﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MultipleBackendRepositories.Domain;
using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.Infrastructure.EF
{
    public class SqlServerUnitOfWork : DbContext, IUnitOfWork
    {
        public SqlServerUnitOfWork() : base("name=MultipleBackendRepositories")
        {
            System.Data.Entity.Database.SetInitializer(new CreateDatabaseIfNotExists<SqlServerUnitOfWork>());
        }

        #region IUnitOfWork Implementations

        public void Add<TEntity>(TEntity obj) where TEntity : class
        {
            Console.WriteLine("Add to SQL");
            base.Set<TEntity>().Add(obj);
        }

        public void Remove<TEntity>(TEntity obj) where TEntity : class
        {
            Console.WriteLine("Remove from SQL");
            base.Set<TEntity>().Remove(obj);
        }

        public void Update<TEntity>(TEntity obj) where TEntity : class
        {
            Console.WriteLine("Update SQL");
            base.Entry(obj).State = EntityState.Modified;
        }

        public IQueryable<TEntity> GetAll<TEntity>() where TEntity : class
        {
            Console.WriteLine("Get from SQL");
            return base.Set<TEntity>().AsQueryable();
        }

        public void Commit()
        {
            base.SaveChanges();
        }

        public void Rollback()
        {
            var entries = base.ChangeTracker.Entries();
            foreach (var entry in entries)
            {
                base.Entry(entry).State = EntityState.Unchanged;
            }

            base.SaveChanges();
        }

        #endregion

        #region DbContext DbSets

        public DbSet<MenuItem> MenuItems { get; set; }
        public DbSet<Payment> Payments { get; set; }

        #endregion
    }
}

﻿using System;

namespace MultipleBackendRepositories.Infrastructure.Database
{
    public interface IAggregateRoot
    {
        Guid Id { get; set; }
    }
}

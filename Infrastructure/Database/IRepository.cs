﻿using System;
using System.Linq;

namespace MultipleBackendRepositories.Infrastructure.Database
{
    public interface IRepository<TEntity>
        where TEntity : class
    {
        TEntity Get(Guid id);
        IQueryable<TEntity> GetAll();
    }
}

﻿using System.Linq;

namespace MultipleBackendRepositories.Infrastructure.Database
{
    public interface IUnitOfWork
    {
        void Add<TEntity>(TEntity obj) where TEntity : class;
        void Remove<TEntity>(TEntity obj) where TEntity : class;
        void Update<TEntity>(TEntity obj) where TEntity : class;
        IQueryable<TEntity> GetAll<TEntity>() where TEntity : class;

        void Commit();
        void Rollback();
    }
}

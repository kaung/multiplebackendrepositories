﻿using System;
using System.Linq;
using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.Domain.Implementations
{
    public class MenuItemRepository : IMenuItemRepository
    {
        private readonly IUnitOfWork _unitOfWork;

        public MenuItemRepository(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException("unitOfWork");

            _unitOfWork = unitOfWork;
        }

        public MenuItem GetActiveMenuItemByName(string name)
        {
            return GetAll().FirstOrDefault(m => m.IsActive && m.Name == name);
        }

        public MenuItem Get(Guid id)
        {
            return GetAll().FirstOrDefault(m => m.Id == id);
        }

        public IQueryable<MenuItem> GetAll()
        {
            return _unitOfWork.GetAll<MenuItem>();
        }
    }
}

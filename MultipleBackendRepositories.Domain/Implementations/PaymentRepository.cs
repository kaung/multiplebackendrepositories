﻿using System;
using System.Linq;
using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.Domain.Implementations
{
    public class PaymentRepository : IPaymentRepository
    {
        private readonly IUnitOfWork _unitOfWork;

        public PaymentRepository(IUnitOfWork unitOfWork)
        {
            if (unitOfWork == null)
                throw new ArgumentNullException("unitOfWork");

            _unitOfWork = unitOfWork;
        }

        public Payment Get(Guid id)
        {
            return GetAll().FirstOrDefault(p => p.Id == id);
        }

        public IQueryable<Payment> GetAll()
        {
            return _unitOfWork.GetAll<Payment>();
        }
    }
}

﻿using System;
using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.Domain
{
    public class Payment : IAggregateRoot
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.Domain
{
    public class MenuItem : IAggregateRoot
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}

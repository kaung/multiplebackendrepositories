﻿using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.Domain
{
    public interface IMenuItemRepository : IRepository<MenuItem>
    {
        MenuItem GetActiveMenuItemByName(string name);
    }
}

﻿using MultipleBackendRepositories.Infrastructure.Database;

namespace MultipleBackendRepositories.Domain
{
    public interface IPaymentRepository : IRepository<Payment>
    {
    }
}
